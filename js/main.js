var danhSachSinhVien = [];

var validatorSv = new ValidatorSV();

const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

const timKiemViTri = function (id, array) {
  return array.findIndex(function (sv) {
    return sv.maSv == id;
  });
};

const luuLocalStorage = function () {
  var dssvJson = JSON.stringify(danhSachSinhVien);

  localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
};

// lấy dữ liệu từ localStorage khi user tải lại trang

var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);

//  gán cho array gốc và render lại giao diện
if (dssvJson) {
  danhSachSinhVien = JSON.parse(dssvJson);
  danhSachSinhVien = danhSachSinhVien.map(function (item) {
    return new SinhVien(
      item.maSv,
      item.tenSv,
      item.emailSv,
      item.diemToan,
      item.diemHoa,
      item.diemLy
    );
  });
  xuatDanhSachSinhVien(danhSachSinhVien);
}

function themSinhVien() {
  var newSinhVien = layThongTinTuForm();

  var isValid = true;

  var isValidMaSv =
    validatorSv.kiemTraRong("txtMaSV", "spanMaSV", "Mã sinh viên chưa nhập") &&
    validatorSv.kiemTraIdHopLe(newSinhVien, danhSachSinhVien);

  var isValidEmail = validatorSv.kiemTraEmail("txtEmail", "spanEmailSV");

  var isValidTenSV =
    validatorSv.kiemTraRong(
      "txtTenSV",
      "spanTenSV",
      "Tên sinh viên chưa nhập"
    ) && validatorSv.kiemTraIdHopLe(newSinhVien, danhSachSinhVien);

  isValid = isValidMaSv && isValidEmail && isValidTenSV;

  if (isValid) {
    danhSachSinhVien.push(newSinhVien);
    xuatDanhSachSinhVien(danhSachSinhVien);
    // convert array thành json để có thể lưu vào localStorage

    luuLocalStorage();
    // console.log(dssvJson);
  }

  //   console.log(danhSachSinhVien);
}

function xoaSinhVien(id) {
  // console.log(id);
  var viTri = timKiemViTri(id, danhSachSinhVien);
  // console.log({ viTri });
  // xoá tại ví trị tìm thấy với số lượng là 1
  danhSachSinhVien.splice(viTri, 1);
  xuatDanhSachSinhVien(danhSachSinhVien);
  luuLocalStorage();
}

function suaSinhVien(id) {
  var viTri = timKiemViTri(id, danhSachSinhVien);

  var sinhVien = danhSachSinhVien[viTri];

  xuatThongTinLenForm(sinhVien);
}
function capNhatSinhVien() {
  var sinhVienEdit = layThongTinTuForm();

  let viTri = timKiemViTri(sinhVienEdit.maSv, danhSachSinhVien);

  danhSachSinhVien[viTri] = sinhVienEdit;

  xuatDanhSachSinhVien(danhSachSinhVien);

  luuLocalStorage();
}
// reset form
function resetForm() {
  document.getElementById("formQLSV").reset();
}
// search sinh viên
function searchItem() {
  let sinhVienValue = document.getElementById("txtSearch").value.toLowerCase();
  let searchSV = danhSachSinhVien.filter(function (item) {
    return item.tenSv.toLowerCase() == sinhVienValue;
  });

  xuatDanhSachSinhVien(searchSV);
}
